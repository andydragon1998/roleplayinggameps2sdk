#ifndef NPC_HANDLER
#define NPC_HANDLER

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>
#include <stdbool.h>

typedef struct
{
	char* Name;
	char* Portrait;
	char* Text;
	bool isShop;
	bool isEngaged;
	u16 posX;
	u16 posY;
	u16 ID;
	
} npcCharacter;


void InitializeNPC(GSGLOBAL* gsGlobal);

void UpdateNPC(GSGLOBAL *gsGlobal);

void DrawNPC(GSGLOBAL *gsGlobal, u64 colour, u16 posX, u16 posY, u16 ID);

void DrawDialogue(GSGLOBAL *gsGlobal, u64 colour, u16 ID, char* text);

#endif
