#ifndef PLAYER
#define PLAYER

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>
#include <stdbool.h>

typedef struct
{
	
	char* Name;
	u16 Class; // Future Enum
	u16 HP;
	u16 MP;
	u16 STR;
	u16 DEF;
	u16 MAG;
	u16 RES;
	u16 AGI;
	u16 Weapon;
	u16 Armour;
	u16 Shield;
	u16 Healmet;
	u16 Boots;
	u16 posX;
	u16 posY;
	u16 width;
	u16 height;
	u16 Status; // Future Enum
	u16 SpriteRow; // 0, 36, 72, 108
	u16 SpriteCollum; //0, 32, 64
	u16 InteractionPointX;
	u16 InteractionPointY;
	bool isEngaged;
} PlayerCharacter;


void InitializePlayer(GSGLOBAL* gsGlobal);

void UpdatePlayer(GSGLOBAL *gsGlobal);

void DrawPlayer(GSGLOBAL *gsGlobal, u64 colour);

#endif
