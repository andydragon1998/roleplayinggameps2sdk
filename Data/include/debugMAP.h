#ifndef DEBUG_MAP
#define DEBUG_MAP

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>
#include "tilesetManager.h"
#include "NPC.h"
#include "Player.h"


void InitializeMap(GSGLOBAL* gsGlobal);

void LoadMapIntoArray(char* path, char* path2);

void UpdateMap(GSGLOBAL *gsGlobal);

void DrawMap(GSGLOBAL *gsGlobal, u64 colour, u16 xOffset, u16 yOffset);

#endif
