#ifndef FONT_MANAGER
#define FONT_MANAGER

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>

typedef struct
{
	u16 posX;
	u16 posY;
	char character;
} Fonty;

void InitializeRPGFont(GSGLOBAL* gsGlobal);

void UpdateRPGFont(GSGLOBAL *gsGlobal);

void DrawRPGFONT(GSGLOBAL *gsGlobal, char* text, u16 posX, u16 posY);

#endif
