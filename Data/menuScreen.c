#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"
#include "include/musicManager.h"
#include "include/pad.h"

#include <stdio.h>
#include <malloc.h>

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>

extern StateMachine GameMachineState;
extern Controller PlaystationGamePad;

GSTEXTURE MenuPicture;

BGM MenuMusic;

extern char* root;

u64 MenuPicColour;

float moveSpeed = 2.0f;

void MenuStart(GSGLOBAL* gsGlobal)
{	
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &MenuPicture, strcat(tempRoot,"Graphics/Images/Title.png"));
	MenuPicColour = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x80,0x00);
	initMusicFormat();
	
	strncpy(tempRoot, root, strlen(root));
	MenuMusic.fileName = strcat(tempRoot,"Audio/BGM/Memoraphile.wav");
	LoadMusic(&MenuMusic);
}

void MenuUpdate(GSGLOBAL* gsGlobal)
{
	// This part here plays the music
	PlayMusic(&MenuMusic);
	
	if(PlaystationGamePad.START_KEY_TAP)
	{
		StateMachineChange(&GameMachineState, &GameState, gsGlobal);
	}
	
}

void MenuDraw(GSGLOBAL* gsGlobal, u64 colour)
{

	gsKit_prim_sprite_texture(gsGlobal, &MenuPicture,	0,  // X1
						0,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						MenuPicture.Width, // X2
						MenuPicture.Height, // Y2
						MenuPicture.Width, // U2
						MenuPicture.Height, // V2
						2,
						MenuPicColour);
}

void MenuEnd(GSGLOBAL* gsGlobal)
{
	printf("This should Run when MenuState is Gone.\n");
	// Clear VRAM after the Menu is done~
	gsKit_vram_clear(gsGlobal);
	UnloadMusic(&MenuMusic);
}

StateManager MenuState =
{
	MenuStart,
	MenuUpdate,
	MenuDraw,
	MenuEnd
};
