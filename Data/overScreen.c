#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"


extern StateMachine GameMachineState;


void OverStart(GSGLOBAL* gsGlobal)
{

}

void OverUpdate(GSGLOBAL* gsGlobal)
{
	
	//StateMachineChange(&GameMachineState, &GameState);
}

void OverDraw(GSGLOBAL* gsGlobal, u64 colour)
{

}

void OverEnd(GSGLOBAL* gsGlobal)
{

}

StateManager OverState =
{
	OverStart,
	OverUpdate,
	OverDraw,
	OverEnd
};
