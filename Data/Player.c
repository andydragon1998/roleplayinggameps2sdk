#include "include/Player.h"
#include "include/pad.h"

PlayerCharacter player = {"AndyTheDragon", 0, 14, 8, 4, 5, 6, 8, 5, 0, 0, 0, 0, 0, 128, 128, 32, 36, 0, 0,0,false};

GSTEXTURE PlayerTexture;

extern Controller PlaystationGamePad;

extern char* root;

u16 frames = 0;


u16 PlayerSpriteMovement[4];

u16 Playermovement;


void InitializePlayer(GSGLOBAL* gsGlobal)
{
	// Player = {Name, Class, HP, MP, STR, DEF, MAG, RES, AGI, W, A, S, H, B, posX, posY, width, height, Status}
	//player = {"AndyTheDragon", 0, 14, 8, 4, 5,  6,8, 5, 0, 0, 0, 0, 0, 48, 48, 32, 36, 0};
	
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &PlayerTexture, strcat(tempRoot,"Graphics/Characters/Healer.png"));
	
	PlayerSpriteMovement[0] = 0;
	PlayerSpriteMovement[1] = 32;
	PlayerSpriteMovement[2] = 64;
	PlayerSpriteMovement[3] = 32;
	
	Playermovement = 0;
}

void UpdatePlayer(GSGLOBAL *gsGlobal)
{
	if(!player.isEngaged)
	{
		if(PlaystationGamePad.LEFT_KEY_DOWN)
		{
			player.posX -= 2;
			player.SpriteRow = 108;
			player.InteractionPointX = player.posX - player.width / 2;
			player.InteractionPointY = player.posY + player.height / 2;
			
		}
		
		if(PlaystationGamePad.RIGHT_KEY_DOWN)
		{
			player.posX += 2;
			player.SpriteRow = 36;
			player.InteractionPointX = player.posX + player.width *2 - player.width /2;
			player.InteractionPointY = player.posY + player.height / 2;
		}
		
		if(PlaystationGamePad.UP_KEY_DOWN)
		{
			player.posY -= 2;
			player.SpriteRow = 0;
			player.InteractionPointX = player.posX + player.width / 2;
			player.InteractionPointY = player.posY - player.height / 2;
		}
		
		if(PlaystationGamePad.DOWN_KEY_DOWN)
		{
			player.posY += 2;
			player.SpriteRow = 72;
			player.InteractionPointX = player.posX + player.width / 2;
			player.InteractionPointY = player.posY + player.height *2 - player.height /2;
		}
	}
}

void DrawPlayer(GSGLOBAL *gsGlobal, u64 colour)
{
	
	frames++;
	
	// This part here does some calculations for the frames that are to be used;
	if(frames > 15)
	{
		player.SpriteCollum = PlayerSpriteMovement[Playermovement];
		
		Playermovement++;
		if(Playermovement > 3)
		{
			Playermovement = 0;
		} 
		frames = 0;
	}
	
	//player.SpriteCollum +=32;
	//player.SpriteCollum %= 96;
	gsKit_prim_sprite_texture(gsGlobal, &PlayerTexture ,player.posX ,  // X1
						player.posY,  // Y2
						player.SpriteCollum,  // U1
						player.SpriteRow,  // V1
						player.width + player.posX, // X2
						player.height + player.posY, // Y2
						player.SpriteCollum + 32,  // U1
						player.SpriteRow + 36,  // V2			
						2,
						colour);
						
	
}
