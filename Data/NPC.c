#include "include/NPC.h"

GSTEXTURE CharacterTextures[5];
u16 SpriteRows[5]; // 0, 36, 72, 108
u16 SpriteCollums[5]; //0, 32, 64

u16 SpriteMovement[4];

u16 movement;

GSTEXTURE DialogueBox;

extern char* root;

u16 NPCframes = 0;

// This part here initializes and handles all the graphics of the NPC's in the are that are used.
void InitializeNPC(GSGLOBAL* gsGlobal)
{
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &CharacterTextures[0], strcat(tempRoot,"Graphics/Characters/Warrior.png"));
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &CharacterTextures[1], strcat(tempRoot,"Graphics/Characters/Mage.png"));
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &CharacterTextures[2], strcat(tempRoot,"Graphics/Characters/CuteGirl.png"));
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &CharacterTextures[3], strcat(tempRoot,"Graphics/Characters/Elder.png"));
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &CharacterTextures[4], strcat(tempRoot,"Graphics/Characters/Healer.png"));
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &DialogueBox, strcat(tempRoot,"Graphics/GUI/TextFrame.png"));
	SpriteRows[0] = 36;
	SpriteRows[1] = 72;
	SpriteRows[2] = 72;
	SpriteRows[3] = 36;
	SpriteRows[4] = 0;
	
	SpriteMovement[0] = 0;
	SpriteMovement[1] = 32;
	SpriteMovement[2] = 64;
	SpriteMovement[3] = 32;
	movement = 0;
}

void UpdateNPC(GSGLOBAL *gsGlobal)
{
	NPCframes++;
	
}

void DrawNPC(GSGLOBAL *gsGlobal, u64 colour, u16 posX, u16 posY, u16 ID)
{
	
	
	// This part here does some calculations for the frames that are to be used;
	if(NPCframes > 15)
	{
		SpriteCollums[0] =SpriteMovement[movement];
		SpriteCollums[1] =SpriteMovement[movement];
		SpriteCollums[2] =SpriteMovement[movement];
		SpriteCollums[3] =SpriteMovement[movement];
		SpriteCollums[4] =SpriteMovement[movement];
		
		movement++;
		
		if(movement > 3)
		{
			movement = 0;
		}
		NPCframes = 0;
	}
	
	//player.SpriteCollum +=32;
	//player.SpriteCollum %= 96;
	gsKit_prim_sprite_texture(gsGlobal, &CharacterTextures[ID] ,posX ,  // X1
						posY,  // Y2
						SpriteCollums[ID],  // U1
						SpriteRows[ID],  // V1
						CharacterTextures[ID].Width /3 + posX, // X2
						CharacterTextures[ID].Height /4 + posY, // Y2
						SpriteCollums[ID] + 32,  // U1
						SpriteRows[ID] + 36,  // V2			
						2,
						colour);
}

// This part here draws the dialogue that occurs between player and the NPC
void DrawDialogue(GSGLOBAL *gsGlobal, u64 colour, u16 ID, char* text)
{
	gsKit_prim_sprite_texture(gsGlobal, &DialogueBox ,40 ,  // X1
						360,  // Y2
						0,  // U1
						0,  // V1
						600, // X2
						500, // Y2
						DialogueBox.Width,  // U1
						DialogueBox.Height,  // V2			
						2,
						colour);
}


